package com.guruofjava.springbootwiki.model;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SectionRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public void addSection(Section section){
        
    }
    
    public void updateSection(Section section){
        
    }
    
    public void deleteSection(Integer sectionId){
        
    }
    
    public Section getSectionById(Integer sectionId){
        return null;
    }
    
    public List<Section> getSectionsByArticleVersionId(Integer articleVersionId){
        return null;
    }
}
