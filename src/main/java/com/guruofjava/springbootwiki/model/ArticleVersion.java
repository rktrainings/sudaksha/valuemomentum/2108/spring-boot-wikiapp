package com.guruofjava.springbootwiki.model;

import java.time.LocalDateTime;

public class ArticleVersion {
    private Integer articleVersionId;
    private Integer version;
    private String summary;
    private Integer articleId;
    private LocalDateTime lastModifiedOn;
    private Boolean active;
    
    public ArticleVersion(){
        super();
    }

    public ArticleVersion(Integer articleVersionId, Integer version, String summary, 
            Integer articleId, LocalDateTime lastModifiedOn, Boolean active) {
        this.articleVersionId = articleVersionId;
        this.version = version;
        this.summary = summary;
        this.articleId = articleId;
        this.lastModifiedOn = lastModifiedOn;
        this.active = active;
    }

    public Integer getArticleVersionId() {
        return articleVersionId;
    }

    public void setArticleVersionId(Integer articleVersionId) {
        this.articleVersionId = articleVersionId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public LocalDateTime getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(LocalDateTime lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
