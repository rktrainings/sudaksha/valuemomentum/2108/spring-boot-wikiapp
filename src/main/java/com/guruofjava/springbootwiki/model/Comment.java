package com.guruofjava.springbootwiki.model;

import java.time.LocalDateTime;

public class Comment {

    private Integer commentId;
    private Integer articleVersionId;
    private String body;
    private LocalDateTime postedAt;
    private Integer postedTo;

    public Comment() {
        super();
    }

    public Comment(Integer commentId, Integer articleVersionId, String body, 
            LocalDateTime postedAt, Integer postedTo) {
        this.commentId = commentId;
        this.articleVersionId = articleVersionId;
        this.body = body;
        this.postedAt = postedAt;
        this.postedTo = postedTo;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getArticleVersionId() {
        return articleVersionId;
    }

    public void setArticleVersionId(Integer articleVersionId) {
        this.articleVersionId = articleVersionId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(LocalDateTime postedAt) {
        this.postedAt = postedAt;
    }

    public Integer getPostedTo() {
        return postedTo;
    }

    public void setPostedTo(Integer postedTo) {
        this.postedTo = postedTo;
    }
}
