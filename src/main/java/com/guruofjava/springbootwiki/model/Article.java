package com.guruofjava.springbootwiki.model;

import java.time.LocalDateTime;

public class Article {

    private Integer articleId;
    private LocalDateTime publishedAt;
    private String keywords;
    private String title;
    private String user;

    public Article() {
        super();
    }

    public Article(Integer articleId, LocalDateTime publishedAt, String keywords,
            String title, String user) {
        this.articleId = articleId;
        this.publishedAt = publishedAt;
        this.keywords = keywords;
        this.title = title;
        this.user = user;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
