package com.guruofjava.springbootwiki.model.mappers;

import com.guruofjava.springbootwiki.model.Comment;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CommentRowMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int i) throws SQLException {
        return new Comment(rs.getInt(1), rs.getInt(2), rs.getString(3),
                rs.getTimestamp(4).toLocalDateTime(), rs.getInt(5));
    }

}
