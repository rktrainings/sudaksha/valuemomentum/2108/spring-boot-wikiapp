package com.guruofjava.springbootwiki.model.mappers;

import com.guruofjava.springbootwiki.model.ArticleVersion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ArticleVersionRowMapper implements RowMapper<ArticleVersion> {
    
    @Override
    public ArticleVersion mapRow(ResultSet rs, int i) throws SQLException {
        return new ArticleVersion(rs.getInt(1), rs.getInt(2), rs.getString(3),
                rs.getInt(4), rs.getTimestamp(5).toLocalDateTime(), rs.getBoolean(6));
    }
    
}
