package com.guruofjava.springbootwiki.model.mappers;

import com.guruofjava.springbootwiki.model.Article;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ArticleRowMapper implements RowMapper<Article> {

    @Override
    public Article mapRow(ResultSet rs, int i) throws SQLException {
        return new Article(rs.getInt(1), rs.getTimestamp(2).toLocalDateTime(),
                rs.getString(3), rs.getString(4), rs.getString(5));
    }

}
