package com.guruofjava.springbootwiki.model.mappers;

import com.guruofjava.springbootwiki.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        return new User(rs.getString(1), rs.getString(2),
                rs.getString(3), rs.getString(4), rs.getBoolean(5));
    }

}
