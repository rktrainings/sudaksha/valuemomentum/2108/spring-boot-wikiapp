package com.guruofjava.springbootwiki.model.mappers;

import com.guruofjava.springbootwiki.model.Section;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SectionRowMapper implements RowMapper<Section> {

    @Override
    public Section mapRow(ResultSet rs, int i) throws SQLException {
        return new Section(rs.getInt(1), rs.getString(2), rs.getString(3),
                rs.getInt(4));
    }

}
