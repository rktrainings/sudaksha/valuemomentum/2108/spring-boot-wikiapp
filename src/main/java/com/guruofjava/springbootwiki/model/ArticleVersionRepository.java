package com.guruofjava.springbootwiki.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleVersionRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public void addArticleVersion(ArticleVersion articleVersion){
        
    }
    
    public void updateArticleVersion(ArticleVersion articleVersion){
        
    }
        
    public void publishArticleVersion(Integer articleVersionId){
        
    }
    
}
