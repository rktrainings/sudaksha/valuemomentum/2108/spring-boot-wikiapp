package com.guruofjava.springbootwiki.model;

import com.guruofjava.springbootwiki.model.mappers.UserRowMapper;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Optional<User> getUserById(String username) {
        try {
            return Optional.of(jdbcTemplate.queryForObject("SELECT * FROM users WHERE username=?",
                    new UserRowMapper(), username));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
    }

    public void addUser(User user) {
        jdbcTemplate.update("INSERT INTO users VALUES(?, ?, ?, ?, ?)",
                user.getUsername(), user.getPassword(), user.getName(),
                user.getMobile(), user.getEnabled());
    }
}
