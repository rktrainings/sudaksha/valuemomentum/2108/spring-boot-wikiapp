package com.guruofjava.springbootwiki.model;

public class Section {

    private Integer sectionId;
    private String title;
    private String body;
    private Integer articleVersionId;

    public Section() {
        super();
    }

    public Section(Integer sectionId, String title, String body, 
            Integer articleVersionId) {
        this.sectionId = sectionId;
        this.title = title;
        this.body = body;
        this.articleVersionId = articleVersionId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getArticleVersionId() {
        return articleVersionId;
    }

    public void setArticleVersionId(Integer articleVersionId) {
        this.articleVersionId = articleVersionId;
    }
}
