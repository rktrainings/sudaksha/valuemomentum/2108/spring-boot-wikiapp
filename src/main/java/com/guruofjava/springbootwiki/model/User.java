package com.guruofjava.springbootwiki.model;

public class User {

    private String username;
    private String password;
    private String name;
    private String mobile;
    private Boolean enabled;

    public User() {
        super();
    }

    public User(String username, String password, String name, String mobile,
            Boolean enabled) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.mobile = mobile;
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
