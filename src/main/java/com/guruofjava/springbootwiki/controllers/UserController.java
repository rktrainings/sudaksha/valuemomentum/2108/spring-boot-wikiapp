package com.guruofjava.springbootwiki.controllers;

import com.guruofjava.springbootwiki.model.User;
import com.guruofjava.springbootwiki.model.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    @Autowired
    private PasswordEncoder pwdEncoder;

    @Autowired
    private UserRepository userRepo;

    @PostMapping("/user")
    public String addUser(User user) {
        user.setPassword(pwdEncoder.encode(user.getPassword()));
        userRepo.addUser(user);
        return "home";
    }

    @GetMapping("/user/{username}")
    public String getUserProfile(Model model, @PathVariable("username") String username) {

        Optional<User> userOptional = userRepo.getUserById(username);

        model.addAttribute("userObject",
                userOptional.orElseThrow(() -> new RuntimeException("No user by that name")));

//        if (user.isPresent()) {
//            //model.addAttribute("userObject", user.get());
//            //model.addAttribute("userObject", user.orElse(new User()));
//        } else {
//            model.addAttribute("error", "Given user is not present");
//        }
        return "userPages/viewUserProfile";
    }
}
