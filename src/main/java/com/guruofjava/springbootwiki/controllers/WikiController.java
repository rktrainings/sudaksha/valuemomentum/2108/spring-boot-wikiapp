package com.guruofjava.springbootwiki.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WikiController {
    @RequestMapping
    public String home(){
        return "home";
    }
    
    @RequestMapping("/home")
    public String homeAgain(){
        return "home";
    }
}
