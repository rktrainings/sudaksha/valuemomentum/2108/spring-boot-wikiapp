<%-- 
    Document   : home
    Created on : 20-Sep-2021, 7:48:59 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="template/header.jsp"/>
    </head>
    <body>
        <jsp:include page="template/banner.jsp"/>

        <div class="containerBox">

            <div class="containerBox">
                <div class="articleBox">
                    <h2>Article Title</h2>
                    <p>
                        Article Content<br/><br/> .... 
                    </p>
                </div>
                <div class="articleBox">
                    <h2>Article Title</h2>
                    <p>
                        Article Content<br/><br/> .... 
                    </p>
                </div>
            </div>
        </div>


        <jsp:include page="template/footer.jsp"/>
    </body>
</html>
